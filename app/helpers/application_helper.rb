module ApplicationHelper

  def facebook_share_url url, msj
    "https://www.facebook.com/sharer/sharer.php?u=#{url}&title=#{msj}"
  end

  def twitter_share_url url, msj
    "https://www.twitter.com/intent/tweet?url=#{url}&text=#{msj}"
  end

  def social_share url=request.original_url, msj='Mira esto!'
    "<a  href='#{facebook_share_url url, msj}' target='_blank'>  <i class='fa fa-facebook fa-lg'></i> </a>".html_safe +
    "<a target='_blank' href='#{twitter_share_url url, msj}'><i class='fa fa-twitter fa-lg'></i> </a>".html_safe
  end

end

class StaticsController < ApplicationController
  def home
    @tema = Theme.last
    redirect_to "/ideas/#{@tema.slug}/proponer"
  end
end

class IdeasController < ApplicationController

  before_action :idea_params , :only => [:create]

  before_action :set_theme, :only => [:la_verdad,:new,:index,:mas_votadas]

  def index
    @ideas = @theme.ideas.paginate(:page => params[:page], :per_page => 8)
  end

  def create
    @idea = Idea.new(idea_params)

    if @idea.save
      redirect_to "/ideas/#{@idea.theme.slug}/mas/1"
    else
      flash[:danger] = @idea.errors
      redirect_to new_idea_path(@idea.theme.slug)
    end
  end

  def new
    @idea = Idea.new
  end

  def mas_votadas
    @ideas = @theme.ideas.order(votePlus: :desc).paginate(:page => params[:page], :per_page => 8)
    render 'ideas/index'
  end

  def show
    @idea = Idea.find(params[:id])
  end

  def la_verdad

  end

  private

  def set_theme
    @theme =  Theme.find_by(:slug => params[:slug])
  end

  def idea_params
    params.require(:idea).permit(:content,:theme_id)
  end
end

class VotesController < ApplicationController

  before_action :set_idea

  def create
    counter = @idea.votePlus
    @idea.update({votePlus: counter +=  1})

  end

  def destroy

    counterLess = @idea.voteLess
    @idea.update({voteLess: counterLess +=  1})
  end

  private

  def set_idea
    @idea = Idea.find(params[:id])
  end

end

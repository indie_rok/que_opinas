class Idea < ApplicationRecord
  belongs_to :theme

  validates :content,length: { minimum: 6, maximum:1000 }
  validates :content, presence: true, uniqueness: true
end

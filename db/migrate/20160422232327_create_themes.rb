class CreateThemes < ActiveRecord::Migration[5.0]
  def change
    create_table :themes do |t|
      t.string :title, presence: true
      t.string :slug, presence: true
      
      t.timestamps
    end
  end
end

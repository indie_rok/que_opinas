class CreateIdeas < ActiveRecord::Migration[5.0]
  def change
    create_table :ideas do |t|
      t.string :content, null: false
      t.integer :votePlus, default: 0
      t.integer :voteLess, default: 0
      t.integer :theme_id

      t.timestamps
    end
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

themes = Theme.create([{title: 'La libertad de expreción', slug: 'libertad-de-expresion'}])

ideas = Idea.create([
  {content: 'es seguir a tu mejor amigo', votePlus: 3, voteLess:0 ,theme: themes.first},
  {content: 'es bailar', votePlus: 0, voteLess:2, theme: themes.first},
  {content: 'es probar caracteres latinoamericanos ò ì è à ñ',votePlus: 2, voteLess: 6, theme: themes.first},
  {content: 'es seguir a tu mejor amigo', votePlus: 4, voteLess:77 , theme: themes.first},
  {content: 'Al contrario del pensamiento popular, el texto de Lorem Ipsum
    no es simplemente texto aleatorio. Tiene sus raices en una pieza cl´sica
    de la literatura del Latin, que data del año 45 antes de Cristo, haciendo
    que este adquiera mas de 2000 años de antiguedad. ', votePlus: 3, voteLess:5, theme: themes.first}
])

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'

  root to:'statics#home'
  resources :ideas, only: [:create]

  get 'ideas/ver/:id', to: 'ideas#show', as: 'ver_idea'
  get 'ideas/:slug/mas/:page', to: 'ideas#index', as: 'idea'
  get 'ideas/:slug/votadas/:page', to:'ideas#mas_votadas', as: 'mas_votadas'
  get 'ideas/:slug/proponer', to: 'ideas#new', as: 'new_idea'
  get 'ideas/:slug/la_verdad', to: 'ideas#la_verdad', as: 'la_verdad'

  post 'votes/:id', to:'votes#create', as: 'vote'
  delete 'votes/:id', to:'votes#destroy'

end
